Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Rob Kinyon <rob.kinyon@gmail.com>
Source: https://metacpan.org/release/Excel-Template
Upstream-Name: Excel-Template

Files: *
Copyright: 2010-2012, Robert Bohne <rbo@cpan.org>
 2003-2007, Rob Kinyon <rob.kinyon@gmail.com>
 2012, Robert James Clay <jame@rocasa.us>
 2011, Nigel Metheringham <nigelm@cpan.org>
 2007, Jens Gassmann <jegade@cpan.org>
License: Artistic or GPL-1+
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

Files: debian/*
Copyright: 2011-2017, Robert James Clay <jame@rocasa.us>
 2012-2016, gregor herrmann <gregoa@debian.org>
 2013-2016, Salvatore Bonaccorso <carnil@debian.org>
License: GPL-2+

Files: inc/Module/*
Copyright: 2002-2011, Adam Kennedy <adamk@cpan.org>
 2002-2011, Audrey Tang <autrijus@autrijus.org>
 2002-2011, Brian Ingerson <ingy@cpan.org>
License: Artistic or GPL-1+

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 dated June, 1991, or (at your
 option) any later version.
 .
 On Debian systems, the complete text of version 2 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-2'.

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.
